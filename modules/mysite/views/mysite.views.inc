<?php

/**
 * @file
 * Views handlers for mysite.module.
 */

/**
 * Implements hook_views_data_alter().
 */
function mysite_views_data_alter(&$data) {
  // Add a dummy field on title that will
  // return the path to the node if the body field is not empty.
  $data['node']['title_token'] = array(
    'group' => t('Content'),
    'title' => t('Title Token'),
    'help' => t('Return a tokenized version of the title'),
    'real field' => 'title',
    'field' => array(
      'handler' => 'mysite_handler_title_token',
      'click sortable' => FALSE,
    ),
  );
}

/**
 * Return a path if the body is not empty.
 *
 * @ingroup views_filter_handlers
 */
class mysite_handler_title_token extends views_handler_field_node {
  /**
  * Render the field.
  */
  public function render($values) {
    $value = $this->get_value($values);
    if (!empty($value)) {
      module_load_include('inc', 'pathauto', 'pathauto');
      return pathauto_cleanstring($value);
    }
  }
}
