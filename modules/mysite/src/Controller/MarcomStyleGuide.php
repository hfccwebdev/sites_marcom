<?php

/**
 * Provides an endpoint to export style guide content.
 */
class MarcomStyleGuide {

  public static function export(): void {

    drupal_add_http_header('Content-Type', 'text/plain; charset=UTF-8');

    $nids = db_query("
      SELECT nid FROM {node}
      WHERE status = 1 AND type = 'style'
      ORDER BY title
    ")->fetchCol();

    foreach (node_load_multiple($nids) as $node) {
      $token = drupal_clean_css_identifier($node->title);
      echo "## {$node->title} ## {#{$token}}\n\n";
      echo $node->body[LANGUAGE_NONE][0]['value'] . "\n\n";
    }

    drupal_exit();
  }
}
